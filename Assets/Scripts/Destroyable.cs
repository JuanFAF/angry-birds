using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class Destroyable : MonoBehaviour
{
    [SerializeField] private float destroyDelay;

    new protected Rigidbody2D rigidbody;

    public event Action OnDestroyed;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();

        SetUp();
    }

    public void DestroyDelay(float delay)
    {
        CancelInvoke();
        Invoke(nameof(Destroy), delay);
    }
    public void DestroyDelay()
    {
        DestroyDelay(destroyDelay);
    }

    public void Destroy()
    {
        gameObject.SetActive(false);

        OnDestroyed?.Invoke();
    }

    public virtual void SetUp() 
    {

    }
}
