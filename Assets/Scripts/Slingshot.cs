using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slingshot : MonoBehaviour
{
    [SerializeField] private Transform birdPivot;

    public void SetBird(Bird bird) 
    {
        bird.transform.SetParent(birdPivot);
        bird.transform.localPosition = Vector3.zero;
    }
}
