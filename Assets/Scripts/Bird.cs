using UnityEngine;

[RequireComponent(typeof(SpringJoint2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Bird : Destroyable
{
    const float WOOD_DESTROY_DELAY = 0.3f;
    const float BIRD_DESTROY_DELAY = 1f;
    const float GRAVITY_SCALE = 2.5f;

    private bool isLaunchable;
    private bool? hasBeenThrown;
    private SpringJoint2D springJoint;

    public override void SetUp()
    {
        isLaunchable = false;
        hasBeenThrown = null;
        rigidbody.gravityScale = 0f;

        springJoint = GetComponent<SpringJoint2D>();

        springJoint.enabled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.collider.tag) 
        {
            case "Pig":
                Destroyable pig = collision.gameObject.GetComponent<Pig>();
                pig.Destroy();
                break;

            case "Wood":
                Destroyable wood = collision.gameObject.GetComponent<Wood>();
                wood.DestroyDelay(WOOD_DESTROY_DELAY);
                break;

            case "Ground":
                if(hasBeenThrown == true) 
                {
                    DestroyDelay(BIRD_DESTROY_DELAY);
                }
                break;
        }
    }

    private void Update()
    {
        DetectTouch();
        DetectLaunchLimit();
    }

    private void DetectTouch()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Launch();
        }

        if (Input.GetMouseButton(0) && isLaunchable == true)
        {
            Vector2 mousePosition = Input.mousePosition;
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

            transform.position = new Vector3(worldPosition.x, worldPosition.y, transform.position.z);
        }
    }

    private void Launch() 
    {
        if (hasBeenThrown == null) return;
        
        hasBeenThrown = true;
        rigidbody.isKinematic = false;
        rigidbody.gravityScale = GRAVITY_SCALE;
    }

    private void DetectLaunchLimit() 
    {
        if (transform.position.x > springJoint.connectedBody.transform.position.x && hasBeenThrown == true)
        {
            springJoint.enabled = false;
        }
    }

    public void MakeLaunchable() 
    {
        springJoint.enabled = true;
        isLaunchable = true;
        hasBeenThrown = false;
        rigidbody.isKinematic = true;
    }
}
