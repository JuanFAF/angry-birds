using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Queue<Bird> birds;
    [SerializeField] private Slingshot slingshot;
    [SerializeField] private Transform levelsParent;

    private void Start()
    {
        birds = new Queue<Bird>();
    }

    private void SetNewBird() 
    {
        Bird bird = default;
        if(birds.TryDequeue(out bird)) 
        {
            bird.MakeLaunchable();
            slingshot.SetBird(bird);
        }
    }

    public void SetLevel(int level) 
    {
        if (level <= 0 || level > levelsParent.childCount) return;

        var levelTransform = levelsParent.GetChild(level - 1);

        levelTransform.gameObject.SetActive(true);
        var _birds = levelTransform.GetComponentsInChildren<Bird>();

        foreach (var bird in _birds)
        {
            bird.OnDestroyed += SetNewBird;
            birds.Enqueue(bird);
        }

        SetNewBird();
    }
}
